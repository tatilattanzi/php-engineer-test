# PHP Engineer Test

## Requirements

* PHP 5.6+/7.0+
* MySQL 5.6+
* [Composer](https://getcomposer.org)

## Installation

To setup the project in your machine, run the following commands:

```
$ cd /path/to/project
$ composer install --no-scripts
$ composer run-script post-root-package-install
$ composer run-script post-create-project-cmd
$ composer run-script post-install-cmd
# edit the .env file with an editor of your preference to setup the database credentials
$ vim .env
$ php artisan migrate
```

## The Problem

We've created a nice to-do list with Laravel! But we have some issues. Can you help us?

## The Objective

Given the existing code base, fix all the issues you can find to make this to-do list work.

## Requirements

When we are done with the fixes, we want to add some new cool features, and pay some tech-debt:

- Propose and implement the refactor you find necessary to make working with this codebase more pleasant
- Avoid allowing tasks without a description
- Add a method to check if the description of the task is an anagram of some word given by the system
- Add testing to the app's main logic

## Extra requirements
  
- Make the necessary changes to implement the "Mark task as completed" feature

## Doubts

Ask the interviewer anything you want about the exercise. 

##### Good luck!
